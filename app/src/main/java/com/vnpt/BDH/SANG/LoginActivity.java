package com.vnpt.BDH.SANG;

import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.vnpt.BDH.SANG.network.APIManager;
import com.vnpt.BDH.SANG.request.LoginRequest;
import com.vnpt.BDH.SANG.response.LoginResponse;
import com.vnpt.BDH.SANG.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.vnpt.BDH.SANG.network.APIManager.SERVER_URL;


public class LoginActivity extends AppCompatActivity {
    LoginResponse logininfo;
    LoginRequest loginRequest = new LoginRequest("dfh;aguoafgmagp'gaafa64dgfgsgs6fg44s3g2sf9g","IOS","14843461661464","Abc@1234","testapp1");
    boolean showpass = false;
    @BindView(R.id.ivlogo)
    ImageView ivlogo;
    @BindView(R.id.edusername)
    EditText edusername;
    @BindView(R.id.edpassword)
    EditText edpassword;
    @BindView(R.id.ivshowpass)
    ImageView ivshowpass;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnForget)
    TextView btnForget;
    @BindView(R.id.ivprintfinger)
    ImageView ivprintfinger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ivshowpass, R.id.btnLogin, R.id.btnForget, R.id.ivprintfinger})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivshowpass:
                Showpass();
                break;
            case R.id.btnLogin:
                Login();
                if (logininfo != null && logininfo.getErrorCode() == 0)
                {
                    Log.d("success", "Đăng nhập thành công");
                }
                else
                {
                    Log.d("error", "Đăng nhập thất bại");
                }
                break;
            case R.id.btnForget:
                break;
            case R.id.ivprintfinger:
                break;
        }
    }
    public void Showpass()
    {
        if (showpass)
        {
            ivshowpass.setImageResource(R.drawable.ic_show_password);
            edpassword.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);
            showpass = false;
        }
        else
        {
            ivshowpass.setImageResource(R.drawable.ic_hide_password);
            edpassword.setInputType(InputType.TYPE_CLASS_TEXT);
            showpass = true;
        }

    }
    private void Login()
    {
        Retrofit retrofit = new  Retrofit.Builder().baseUrl(SERVER_URL).addConverterFactory(GsonConverterFactory.create()).build();
        APIManager apiManager = retrofit.create(APIManager.class);
        apiManager.Login(loginRequest).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body() != null)
                {
                    logininfo = response.body();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("TAG", "onFailure: ");
            }
        });
    }
}